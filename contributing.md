## Contributing

First, clone the project:
```
$ git clone https://gitlab.com/ptaiga-edu/mlops3.git
$ cd mlops3
```
(install `git` if it is missing).

### Dependecies

Bellow command help to create `python 3.8` environment with neccessary dependencies.

#### PIP

Install Python 3.8 and perform next commands:

```
$ python3 -m venv env
$ source env/bin/activate
$ pip install -r requirements.txt
```

#### CONDA

Install latest CONDA and perform next commands:
```
> conda env create --name <env> --file requirements-conda.txt
> conda activate <env>
```
Without environment activating, it's required to add a prefix when running every command: `conda run -n <env> <command>`

#### PDM

Command to install all dependencies specified in `pyproject.toml`and check resolved and locked libraries in `pdm.lock`:

```
$ pdm install --check
```

Use flag `--prod` to choose production-only libraries.

PDM will create virtual environment and use it for setup. Scripts manually run by command:
```
pdm run python src/main.py
```

If you want to run scripts without `pdm run` prefix then change the shell by injecting the virtualenv paths to environment variable:
```
$ eval $(pdm venv activate in-project)
```

#### CONDA/MAMBA + PDM

Use `conda.yaml` config create virtual environment and install into it:
 - corresponding version Python and PDM,
 - specified science packages using conga-package format.
Command:
```
$ conda env create -n <env> -f conda.yaml
```
(note: for MAMBA use `mamba create -n <env> -f conda.yaml`).

Install dependencies specified in `pyproject.toml`:
```
$ conda run -n <env> pdm install
```
Activate environment and run scripts:
```
$ conda activate <env>
(<env>)$ pdm run <script>
```
Without environment activating, it's required to add a prefix when running every command: `conda run -n <env> <command>`. For example, run python scripts: `conda run -n <env> pdm run python <script>`.


### Autoformat and linting

#### Autoformat code to PEP8

Use flag `--check` to check problems or flag '--diff' to see what would be changed.
```
$ black .
$ isort .
```
For Jupyter Notebooks:
```
$ nbqa black .
```

#### Lint code

```
$ flake8 .
$ mypy .
```

#### Pre-commit

Also changed files might be checked by `pre-commit` on every commit. It's need to perform `pre-commit install` after dependencies installation.


### Pull request

How to make a pull request.

- Clone the repository `git clone https://gitlab.com/ptaiga-edu/mlops3.git`;
- Create virtual environment and install dependencies. See [`contributing.md`](contributing.md);
- Create a new branch, for example `git checkout -b issue-id-short-name`;
- Make changes to the code (make sure you are definitely working in the new branch);
- Autoformat and lint code. See [`contributing.md`](contributing.md);
- `git push`;
- Create a pull request to the `main` branch;
- Add a brief description of the work done;
- Expect comments from the maintainers or wait for the changes to be approved.
