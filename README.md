# MLOps3 Homeworks

Repository for homework assingments completed as part of MLOps3 course.

[![pdm-managed](https://img.shields.io/badge/pdm-managed-blueviolet)](https://pdm.fming.dev) [![Python version](https://img.shields.io/badge/python-3.8-blue)]() [![License MIT](https://img.shields.io/badge/license-MIT-green)]()


## Contributing

See [`contributing.md`](contributing.md) to detailed description of creating environment, dependencies installation and code linting.


## Gitlab Pages

Use [“NY 2015 Street Tree Census - Tree Data”](https://www.kaggle.com/datasets/new-york-city/ny-2015-street-tree-census-tree-data) dataset.

 - [EDA (NY-Tree-2015)](https://nlpops-ptaiga-edu-d2c4eb4c8445ce36247d117e5ecf3368eb0a04b89c13d.gitlab.io/eda.html) to Block 4 assigment.
 - [Snakemake (DAG)](https://nlpops-ptaiga-edu-d2c4eb4c8445ce36247d117e5ecf3368eb0a04b89c13d.gitlab.io/snakemake.html) to Block 5 Part 1 assigment
 - [DVC (pipeline)](https://nlpops-ptaiga-edu-d2c4eb4c8445ce36247d117e5ecf3368eb0a04b89c13d.gitlab.io/dvc-pipeline.html) to Block 6 Part 1 assigment
 - [MLFlow](https://nlpops-ptaiga-edu-d2c4eb4c8445ce36247d117e5ecf3368eb0a04b89c13d.gitlab.io/mlflow.html) to Block 7 Part 1 assigment
 - [Hypothesis](https://nlpops-ptaiga-edu-d2c4eb4c8445ce36247d117e5ecf3368eb0a04b89c13d.gitlab.io/hypothesis.html) to Block 8 assigment


## Snakemake

Clone repo and change dir.
```
$ git clone https://gitlab.com/ptaiga-edu/mlops3.git
$ cd mlops3/snakemake
```

Validate the pipeline and construct its DAG.
```
$ snakemake -np
$ snakemake --dag | dot -Tsvg > dag.svg
```

Run the pipeline on 8 processor cores and see the results.
```
$ snakemake -c8
$ cat models/combine_score.txt
```

Some folders will be created while the pipeline is running. Remove created folders if they are no longer needed.
```
$ rm -r models processed dataset ny-2015-street-tree-census-tree-data.zip
```


## Hydra

Snakemake's pipeline use Hydra's yaml-config `snakemake/conf/`.

- Dataset config: `snakemake/conf/dataset/`
- Model config: `snakemake/conf/model/`
- Other and default config: `snakemake/conf/config.yaml`

Set environment variable `CPU_CORE` to indicate numbers of processor cores, e.g, `$ export CPU_CORE=4`. By default, `CPU_CORE=8`.


## LakeFS

Example of Snakemake pipeline that use `lakectl` to store and version data into `LakeFS`.

[Download](https://github.com/treeverse/lakeFS/releases) and copy `lakectl` inside `~/.local/bin`

Clone repo, build image and run container:
```
$ git clone https://gitlab.com/ptaiga-edu/mlops3.git
$ cd mlops3/lakefs
$ docker build . -t lakefs
$ docker run --name lakefs --rm --publish 8000:8000 lakefs run --local-settings
```

After that local instance LakeFS is running on http://127.0.0.1:8000/. Auth credentials are in `lakefs/Dockerfile`

Run Snakemake pipeline in new shell:
```
$ cd mlops/lakefs
$ snakemake -c1
```

See results in browser: http://127.0.0.1:8000/repositories/


## Docker

Install _Git_ and _Docker_.

Clone the repository:
```
$ git clone https://gitlab.com/ptaiga-edu/mlops3.git
$ cd mlops3
```
Build image and test it:
```
$ docker build . -t mlops3
$ docker run --rm mlops3
```
You see "All done!" message.

There is ready-made image on [GitLab Registry](https://gitlab.com/ptaiga-edu/mlops3/container_registry). The latest one's path is `registry.gitlab.com/ptaiga-edu/mlops3:latest`

For working inside container:
```
$ docker run -it --rm mlops3 bash
```


## DVC

Clone the repository:
```
$ git clone https://gitlab.com/ptaiga-edu/mlops3.git
$ cd mlops3
```

Create virtual environment (see `dvc/env.yaml`):
```
$ conda env create --name dvc --file dvc/env.yaml
$ conda activate dvc
```

Or work inside Docker container (see `dvc/Dockerfile`):
```
$ docker build ./dvc -t dvc
$ docker run -it --rm dvc bash
```

Inside DVC pipeline used environment variable `$GDRIVE_FOLDER_ID`. Before running scripts set:
```
$ export GDRIVE_FOLDER_ID=<gdrive-folder_id>
```

Config remote storage (Google Drive):
```
$ dvc remote add --local -d gdrive gdrive://$GDRIVE_FOLDER_ID
```

Pull saved data, models, metrics:
```
$ dvc pull
```

Run DVC pipeline from scratch:
```
$ dvc repro --force
```

Looking for MD5-hash changes:
```
$ git diff
```

See Gitlab Pages ["DVC (pipeline)"](https://nlpops-ptaiga-edu-d2c4eb4c8445ce36247d117e5ecf3368eb0a04b89c13d.gitlab.io/dvc-pipeline.html) to get addition information about working with saved data, models and metrics.

The first time access to remote Google Drive (`dvc pull` or `dvc push` command) require pass through special link and perform authorization via Google Account. After that credentials put into file, for example, on Linux: `~/.cache/pydrive2fs/{gdrive_client_id}/default.json`. Credantials could be set into environment variable $GDRIVE_CREDENTIALS_DATA and used on another machine or inside CI/CD pipeline. See ["Authorization"](https://dvc.org/doc/user-guide/data-management/remote-storage/google-drive#authorization) part in ["DVC + Google Drive"](https://dvc.org/doc/user-guide/data-management/remote-storage/google-drive) documentation.


## MLFlow

See [`mlflow/README.md`](mlflow/README.md) and Gitlab Page ["MLFlow"](docs/_site/mlflow.html).


## ClearML

See [`clearml/README.md`](mlflow/README.md) and [ClearML report]](https://app.clear.ml/reports/2b28ade949c049728059864e27894724/1a92121cae3440a58e13cd5af6588044).
