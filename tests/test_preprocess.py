import hypothesis.strategies as st
from hypothesis import given

from mlops.preprocess import new_preprocessing as prep
from mlops.preprocess import preprocessing


@given(st.text(min_size=1))
def test_property_based_hypothesis(text):
    pt = prep(text)
    assert len(pt) <= len(text)
    if pt:
        assert pt.count(" ") == len(pt.split()) - 1
        assert all([t.isalpha() for t in pt.split()])
    if len(pt) >= 3:
        assert len(min(pt.split(), key=len)) >= 3


@given(st.text(min_size=1))
def test_oracle(text):
    assert prep(text, min_word_size=0).split() == preprocessing(text).split()
