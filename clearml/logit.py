import re
from zipfile import ZipFile

import joblib
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.model_selection import train_test_split

from clearml import Dataset, Task


def text_process(input_text: str) -> str:
    text = input_text.lower()
    text = re.sub(
        r'https?://\S+|www\.\S+|\[.*?\]|[^a-zA-Z\s]+|\w*\d\w*', '', text
    )
    text = re.sub(r"[0-9 \-_]+", " ", text)
    text = re.sub("[^a-z A-Z]+", " ", text)
    return text.strip()


def prepare_data(path_to_data):
    zip_file = ZipFile(path_to_data)
    df = pd.read_csv(
        zip_file.open("train.csv"),
        nrows=50000,
        header=None,
        names=['Polarity', 'Title', 'Review'],
    )
    df['Polarity'] = df['Polarity'].map({1: 0, 2: 1})
    df['corpus'] = df['Review'].apply(text_process)
    return df


def vectorize_data(train, test, params):

    tfidf_vectorizer = TfidfVectorizer(**params)

    train_features = tfidf_vectorizer.fit_transform(train)
    test_features = tfidf_vectorizer.transform(test)

    return train_features, test_features, tfidf_vectorizer


def model_run(clf, params, X_train, y_train, X_test, y_test):
    model = clf(**params)

    model.fit(X_train, y_train)
    predicts = model.predict(X_test)

    report = classification_report(y_test, predicts, output_dict=True)
    confusion = confusion_matrix(y_test, predicts)

    return model, report, confusion


def pipeline(model_class, model_params, task_name):
    task = Task.init(
        project_name="Amazon reviews", task_name=task_name, output_uri=True
    )

    task.set_progress(0)

    frame_path = Dataset.get(
        dataset_name="Raw data", dataset_project="Amazon reviews"
    ).get_local_copy()

    task.set_progress(10)

    print("Load and preprocess data...")
    df = prepare_data(frame_path + "/amazon-reviews.zip")

    task.upload_artifact(name="processed_data", artifact_object=df)
    task.set_progress(20)

    print("Split data...")
    train, test = train_test_split(
        df,
        test_size=0.3,
        shuffle=True,
        random_state=13,
    )

    task.set_progress(30)

    print("Vectorize data...")
    vectorizer_params = {"max_features": 10000, "analyzer": "word"}
    train_features, test_features, vectorizer = vectorize_data(
        train["corpus"], test["corpus"], vectorizer_params
    )

    joblib.dump(vectorizer, "vectorizer.pkl", compress=True)
    task.connect(vectorizer_params)
    task.upload_artifact(
        name="train_features",
        artifact_object=(train_features, train["Polarity"].to_numpy()),
    )
    task.upload_artifact(
        name="test_features",
        artifact_object=(test_features, test["Polarity"].to_numpy()),
    )

    task.set_progress(50)

    print("Train and test", task_name, "...")

    model, report, confusion = model_run(
        model_class,
        model_params,
        train_features,
        train["Polarity"],
        test_features,
        test["Polarity"],
    )

    joblib.dump(model, "model.pkl", compress=True)
    task.connect(model_params)

    task.set_progress(80)

    logger = task.get_logger()
    logger.report_single_value("accuracy", report.pop("accuracy"))
    for class_name, metrics in report.items():
        for metric, value in metrics.items():
            if metric == "support":
                continue
            logger.report_single_value(f"{class_name}_{metric}", value)
    logger.report_confusion_matrix(
        "confusion matrix", "ignored", matrix=confusion
    )

    task.set_progress(90)
    print("All done!")
    task.set_progress(100)
    task.close()


if __name__ == "__main__":
    logit_params = {
        "multi_class": "multinomial",
        "solver": "saga",
        "random_state": 13,
    }
    pipeline(LogisticRegression, logit_params, "Logistic Regression")
