import joblib
from scipy.sparse import load_npz

from clearml import InputModel


def main():

    print("Load model...")
    model_pkl = InputModel("cde42bfbfae34fe3bcead44bd9f92dca").get_local_copy()
    model = joblib.load(model_pkl)

    print("Load test sample...")
    test_sample = load_npz("test_sample.npz")

    print("Prediction: ", model.predict(test_sample))


if __name__ == "__main__":
    main()
