# ClearML

Description of the experiments, links to models and comparison plots:

- [ClearML report](https://app.clear.ml/reports/2b28ade949c049728059864e27894724/1a92121cae3440a58e13cd5af6588044).

Here the brief notes about running scripts.


## Create virtual environment

Create and activate `conda` virtual environment:

```bash
$ conda env create -n mlops-clearml -f env.yaml
$ conda activate mlops-clearml
```


## ClearML init

```bash
$ clearml-init
```


## Load dataset

```bash
$ kaggle datasets download -d kritanjalijain/amazon-reviews
$ python clearml_upload_dataset.py
```


## Run experiments

```bash
$ python logit.py
$ python forest.py
```


## Inference

The best model can be loaded and perform prediction with `test_sample.npz`.

```bash
$ python inference.py
```
