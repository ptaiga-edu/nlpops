from logit import pipeline
from sklearn.ensemble import RandomForestClassifier

if __name__ == '__main__':
    forest_params = {
        "n_estimators": 100,
        "random_state": 13,
    }
    pipeline(RandomForestClassifier, forest_params, "Random Forest")
