from clearml import Dataset

dataset = Dataset.create(
    dataset_name="Raw data", dataset_project="Amazon reviews"
)

dataset.add_files(path="amazon-reviews.zip")

dataset.upload()

dataset.finalize()
