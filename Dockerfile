FROM mambaorg/micromamba
# FROM python:3.8.13-slim

WORKDIR /app

COPY conda.yaml ./

RUN micromamba create -n venv -f conda.yaml
# RUN pip install pdm==2.14.0

ENV PDM_CHECK_UPDATE=false

COPY pyproject.toml pdm.lock README.md ./

RUN micromamba run -n venv pdm install --check

COPY ./src/main.py ./

CMD ["micromamba", "run", "-n", "venv", "pdm", "run", "python", "src/main.py"]
