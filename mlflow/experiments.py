import re
from zipfile import ZipFile

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from matplotlib.figure import Figure
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import ConfusionMatrixDisplay, classification_report
from sklearn.model_selection import train_test_split

import mlflow
from mlflow.models import infer_signature


def preprocessing(input_text: str) -> str:
    text = input_text.lower()
    text = re.sub(
        r'https?://\S+|www\.\S+|\[.*?\]|[^a-zA-Z\s]+|\w*\d\w*', '', text
    )
    text = re.sub(r"[0-9 \-_]+", " ", text)
    text = re.sub("[^a-z A-Z]+", " ", text)
    return text.strip()


def conf_matrix(y_true: np.ndarray, pred: np.ndarray) -> Figure:
    plt.ioff()
    fig, ax = plt.subplots(figsize=(5, 5))
    ConfusionMatrixDisplay.from_predictions(
        y_true, pred, ax=ax, colorbar=False
    )
    ax.xaxis.set_tick_params(rotation=90)
    _ = ax.set_title("Confusion Matrix")
    plt.tight_layout()
    return fig


def logit_run():
    run_name = "logistic-regression"

    model_params = {
        "multi_class": "multinomial",
        "solver": "saga",
        "random_state": 13,
    }

    model = LogisticRegression(**model_params)

    with mlflow.start_run(run_name=run_name):

        model.fit(train_features, train["Polarity"])
        predicts = model.predict(test_features)

        report = classification_report(
            test["Polarity"], predicts, output_dict=True
        )
        signature = infer_signature(test_features, test["Polarity"])

        mlflow.log_metric("accuracy", report.pop("accuracy"))
        mlflow.log_metric("f1-weighted", report["weighted avg"]["f1-score"])
        for class_or_avg, metrics_dict in report.items():
            if class_or_avg == 'macro avg':
                break
            for metric, value in metrics_dict.items():
                mlflow.log_metric(class_or_avg + '_' + metric, value)

        mlflow.log_params(vectorizer_params)
        mlflow.log_params(model_params)

        mlflow.sklearn.log_model(
            sk_model=model,
            input_example=test_features[:10],
            artifact_path=f"mlflow/{run_name}/model",
            signature=signature,
            registered_model_name="sk-learn-reg-model",
        )

        fig = conf_matrix(test["Polarity"], predicts)

        mlflow.log_figure(fig, f'{run_name}_confusion_matrix.png')


def forest_run():
    run_name = "random-forest"

    model_params = {
        "n_estimators": 100,
        "random_state": 13,
    }

    model = RandomForestClassifier(**model_params)

    with mlflow.start_run(run_name=run_name):

        model.fit(train_features, train["Polarity"])
        predicts = model.predict(test_features)

        report = classification_report(
            test["Polarity"], predicts, output_dict=True
        )
        signature = infer_signature(test_features, test["Polarity"])

        mlflow.log_metric("accuracy", report.pop("accuracy"))
        mlflow.log_metric("f1-weighted", report["weighted avg"]["f1-score"])
        for class_or_avg, metrics_dict in report.items():
            if class_or_avg == 'macro avg':
                break
            for metric, value in metrics_dict.items():
                mlflow.log_metric(class_or_avg + '_' + metric, value)

        mlflow.log_params(vectorizer_params)
        mlflow.log_params(model_params)

        mlflow.sklearn.log_model(
            sk_model=model,
            input_example=test_features[:10],
            artifact_path=f"mlflow/{run_name}/model",
            signature=signature,
            registered_model_name="sk-learn-reg-model",
        )

        fig = conf_matrix(test["Polarity"], predicts)

        mlflow.log_figure(fig, f'{run_name}_confusion_matrix.png')


if __name__ == "__main__":

    print("Prepare data...")
    zip_file = ZipFile("amazon-reviews.zip")
    df = pd.read_csv(
        zip_file.open("train.csv"),
        nrows=50000,
        header=None,
        names=['Polarity', 'Title', 'Review'],
    )
    df['Polarity'] = df['Polarity'].map({1: 0, 2: 1})
    df['corpus'] = df['Review'].apply(preprocessing)

    print("Train-test split and vectorize data...")
    vectorizer_params = {"max_features": 10000, "analyzer": "word"}
    tfidf_vectorizer = TfidfVectorizer(**vectorizer_params)

    train, test = train_test_split(
        df,
        test_size=0.3,
        shuffle=True,
        random_state=13,
    )

    tfidf_vectorizer.fit(train["corpus"])
    train_features = tfidf_vectorizer.transform(train["corpus"])
    test_features = tfidf_vectorizer.transform(test["corpus"])

    mlflow.set_tracking_uri("http://127.0.0.1:8080")
    experiment = mlflow.set_experiment("TF-IDF_embeddings_50k")

    print("Train and test LogisticRegression...")
    logit_run()
    print("Train and test RandomForestClassifier...")
    forest_run()
    print("All done!")
