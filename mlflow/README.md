# Mlflow

Detailed description of the experiments, model tracking and screenshots, see on Gitlab Pages "[MLFlow](https://nlpops-ptaiga-edu-d2c4eb4c8445ce36247d117e5ecf3368eb0a04b89c13d.gitlab.io/mlflow.html)".

(Local links to this page [`docs/_site/mlflow.html`](docs/_site/mlflow.html) and screenshots `mlops3/docs/images/`)

Here the brief notes about running scripts.


## Create virtual environment

Create and activate `conda` virtual environment:

```bash
$ conda env create -n mlops-mlflow -f env.yaml
$ conda activate mlops-mlflow
```


## Load dataset

```bash
$ kaggle datasets download -d kritanjalijain/amazon-reviews
```


## Run MLFlow server (local mode)

```bash
$ mlflow server --host 127.0.0.1 --port 8080
```


## Run experiments

```bash
$ python experiments.py
```


## Compare models

Inside MLFlow UI (http://127.0.0.1:8080/#/models) compare models and assign alias (e.g., "@champion") to the best one.


## Inference

Choosen model can be loaded and perform prediction with `test_sample.npz`.

```bash
$ python inference.py
```
