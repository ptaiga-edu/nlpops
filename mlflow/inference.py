# import ctypes
# libgcc_s = ctypes.CDLL('libgcc_s.so.1')
# https://stackoverflow.com/questions/64797838/libgcc-s-so-1-must-be-installed-for-pthread-cancel-to-work

from scipy.sparse import load_npz

import mlflow.pyfunc
from mlflow import set_tracking_uri


def main():
    set_tracking_uri("http://127.0.0.1:8080")
    model_name = "sk-learn-reg-model"
    alias = "champion"

    print("Load model...")
    champion_version = mlflow.pyfunc.load_model(
        f"models:/{model_name}@{alias}"
    )

    print("Load test sample...")
    test_sample = load_npz("test_sample.npz")

    print("Prediction: ", champion_version.predict(test_sample))


if __name__ == "__main__":
    main()
