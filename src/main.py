import numpy as np


def main():
    print(" ".join(np.array(["All", "done!"])))
    print("Use: `$ sudo docker run -it --rm <name> bash`")


if __name__ == "__main__":
    main()
