import re


def preprocessing(input_text: str) -> str:
    text = input_text.lower()
    text = re.sub(r'https?://\S+|www\.\S+|\[.*?\]', '', text)
    text = re.sub(r"[0-9 \-_]+", " ", text)
    text = re.sub("[^a-z A-Z]+", " ", text)
    return text.strip()


def new_preprocessing(input_text: str, min_word_size: int = 3) -> str:
    text = input_text.lower()
    text = re.sub(r'https?://\S+|www\.\S+', '', text)
    text = re.sub("[^a-z]+", " ", text)
    if min_word_size and len(text) >= min_word_size:
        text = " ".join([word for word in text.split() if len(word) >= 3])
    return text.strip()


if __name__ == '__main__':
    text = "This sound track was beautiful! It paints the senery in your mind so well I would recomend it even to people who hate vid. game music! I have played the game Chrono Cross but out of all of the games I have ever played it has the best music! It backs away from crude keyboarding and takes a fresher step with grate guitars and soulful orchestras. It would impress anyone who cares to listen! ^_^"  # noqa
    print("Original:", text, "\n")
    print("Processed:", preprocessing(text), "\n")
    print("New processed:", new_preprocessing(text), "\n")
