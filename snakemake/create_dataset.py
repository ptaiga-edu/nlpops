# type: ignore

from zipfile import ZipFile

import pandas as pd

base_features = snakemake.params["base_features"]
extended_features = snakemake.params["extended_features"]

zip_file = ZipFile(snakemake.input[0])
df = pd.read_csv(zip_file.open("2015-street-tree-census-tree-data.csv"))

df_alive_trees = df[df.status == "Alive"][extended_features].dropna()
df_alive_trees.to_csv(snakemake.output["extended_dataset"], index=False)

df_alive_trees = df[df.status == "Alive"][base_features].dropna()
df_alive_trees.to_csv(snakemake.output["base_dataset"], index=False)
