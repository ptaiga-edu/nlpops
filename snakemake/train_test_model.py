# type: ignore

import joblib
import numpy as np
from sklearn.metrics import accuracy_score, f1_score

if snakemake.wildcards.model == 'lr':
    from sklearn.linear_model import LogisticRegression

    clf = LogisticRegression(**snakemake.params["model_params"])
else:
    from sklearn.dummy import DummyClassifier

    clf = DummyClassifier(random_state=12)

X_train = np.loadtxt(snakemake.input[0])
X_test = np.loadtxt(snakemake.input[1])
y_train = np.loadtxt(snakemake.input[2])
y_test = np.loadtxt(snakemake.input[3])

clf.fit(X_train, y_train)
joblib.dump(clf, snakemake.output[0])

pred = clf.predict(X_test)
score = {
    'model': type(clf).__name__,
    'dataset': snakemake.wildcards.dataset,
    'acc': accuracy_score(y_test, pred),
    'f1': f1_score(y_test, pred, average='weighted'),
}

with open(snakemake.output[1], 'a') as f:
    f.write(f"{score}\n")
