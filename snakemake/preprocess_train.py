# type: ignore

import joblib
import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder  # , OneHotEncoder

target_col = snakemake.params["target"]
dataset = pd.read_csv(snakemake.input[0])

target = dataset[target_col]
features = dataset.drop(columns=[target_col])

# oh_enc = OneHotEncoder(sparse_output=False, handle_unknown='ignore')
oh_enc = snakemake.params["oh_enc"]
X = oh_enc.fit_transform(features)

label_enc = LabelEncoder()
y = label_enc.fit_transform(target)

np.savetxt(snakemake.output[0], X)
np.savetxt(snakemake.output[1], y)
joblib.dump(oh_enc, snakemake.output[2])
joblib.dump(label_enc, snakemake.output[3])
