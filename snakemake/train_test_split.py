# type: ignore

import pandas as pd
from sklearn.model_selection import train_test_split

target_col = snakemake.params["target"]

data = pd.read_csv(snakemake.input[0])

train, test = train_test_split(
    data, test_size=0.3, random_state=12, stratify=data[target_col]
)

train.to_csv(snakemake.output[0], index=False)
test.to_csv(snakemake.output[1], index=False)
