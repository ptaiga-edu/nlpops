# type: ignore

import joblib
import numpy as np
import pandas as pd

target_col = snakemake.params["target"]
dataset = pd.read_csv(snakemake.input[0])

target = dataset[target_col]
features = dataset.drop(columns=[target_col])

oh_enc = joblib.load(snakemake.input[1])
X = oh_enc.transform(features)

label_enc = joblib.load(snakemake.input[2])
y = label_enc.transform(target)

# save_sparse_csr(snakemake.output[0], X)
np.savetxt(snakemake.output[0], X)
np.savetxt(snakemake.output[1], y)
