import sys

import joblib
import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sparse_functions import save_sparse_csr

import dvc.api

dataset = sys.argv[1]
X_encoded = sys.argv[2]
y_encoded = sys.argv[3]
oh_enc_model = sys.argv[4]
label_enc_model = sys.argv[5]

params = dvc.api.params_show()
features = params["features"]
target = params["target"]

data = pd.read_csv(dataset)
data_target = data[target]
data_features = data.drop(columns=[target])

oh_enc = OneHotEncoder(sparse_output=True, handle_unknown='ignore')
X = oh_enc.fit_transform(data_features)

label_enc = LabelEncoder()
y = label_enc.fit_transform(data_target)

save_sparse_csr(X_encoded, X)
np.save(y_encoded, y)
joblib.dump(oh_enc, oh_enc_model)
joblib.dump(label_enc, label_enc_model)
