import json
import sys

import joblib
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.figure import Figure
from sklearn.metrics import (  # accuracy_score,; f1_score,
    ConfusionMatrixDisplay,
    classification_report,
)
from sparse_functions import load_sparse_csr


def conf_matrix(y_true: np.ndarray, pred: np.ndarray) -> Figure:
    plt.ioff()
    fig, ax = plt.subplots(figsize=(5, 5))
    ConfusionMatrixDisplay.from_predictions(
        y_true, pred, ax=ax, colorbar=False
    )
    ax.xaxis.set_tick_params(rotation=90)
    _ = ax.set_title("Confusion Matrix")
    plt.tight_layout()
    return fig


X_test_path = sys.argv[1]
y_test_path = sys.argv[2]
model_path = sys.argv[3]
metrics_path = sys.argv[4]
plot_path = sys.argv[5]

X_test = load_sparse_csr(X_test_path)
y_test = np.load(y_test_path)
clf = joblib.load(model_path)

pred = clf.predict(X_test)

# score = {
#     'model': type(clf).__name__,
#     'acc': accuracy_score(y_test, pred),
#     'f1': f1_score(y_test, pred, average='weighted'),
# }
# json.dump(score, open(metrics_path, 'w'))

report = classification_report(y_test, pred, output_dict=True)
json.dump(report, open(metrics_path, 'a'))

fig = conf_matrix(y_test, pred)
plt.savefig(plot_path)
