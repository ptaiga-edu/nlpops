import sys

import joblib
import numpy as np
import pandas as pd
from sparse_functions import save_sparse_csr

import dvc.api

dataset = sys.argv[1]
X_encoded = sys.argv[2]
y_encoded = sys.argv[3]
oh_enc_model = sys.argv[4]
label_enc_model = sys.argv[5]

params = dvc.api.params_show()
features = params["features"]
target = params["target"]

data = pd.read_csv(dataset)
data_target = data[target]
data_features = data.drop(columns=[target])

oh_enc = joblib.load(oh_enc_model)
X = oh_enc.transform(data_features)

label_enc = joblib.load(label_enc_model)
y = label_enc.transform(data_target)

save_sparse_csr(X_encoded, X)
np.save(y_encoded, y)
