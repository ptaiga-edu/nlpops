import sys

import pandas as pd
from sklearn.model_selection import train_test_split

import dvc.api

all_dataset = sys.argv[1]
train_dataset = sys.argv[2]
test_dataset = sys.argv[3]

params = dvc.api.params_show()
target = params["target"]
random_state = params["random_state"]
test_size = params["test_size"]

data = pd.read_csv(all_dataset)

train, test = train_test_split(
    data, test_size=test_size, random_state=random_state, stratify=data[target]
)

train.to_csv(train_dataset, index=False)
test.to_csv(test_dataset, index=False)
