import sys
from zipfile import ZipFile

import pandas as pd

import dvc.api

input_file = sys.argv[1]
output_file = sys.argv[2]

params = dvc.api.params_show()
features = params["features"]
target = params["target"]

zip_file = ZipFile(input_file)
df = pd.read_csv(zip_file.open("2015-street-tree-census-tree-data.csv"))

df_alive_trees = df[df.status == "Alive"][[target] + features].dropna()
df_alive_trees.to_csv(output_file, index=False)
