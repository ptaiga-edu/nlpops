import sys

import joblib
import numpy as np

# from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sparse_functions import load_sparse_csr

import dvc.api

X_train_path = sys.argv[1]
y_train_path = sys.argv[2]
model_path = sys.argv[3]

params = dvc.api.params_show()

# clf = LogisticRegression(**params["logistic_regression"])
clf = RandomForestClassifier(**params["random_forest"])

X_train = load_sparse_csr(X_train_path)
y_train = np.load(y_train_path)

clf.fit(X_train, y_train)
joblib.dump(clf, model_path)
